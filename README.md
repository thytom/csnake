# CSNAKE V2.0
Complete (again) rewrite of CSnake. Less naive, with NCurses, with a cleaner
git repository.

This program was written with ultimate simplicity in mind.

## Fixed Issues
* Apple CAN spawn inside of the snake when the game begins.
	Game now checks that the apple didn't spawn in the snake.
	If it did, it gets re-drawn.
