#include "main.h"

node *create_node(int y, int x)
{
	node *new_node = (node *)malloc(sizeof(node));
	new_node->y    = y;
	new_node->x    = x;
	new_node->next = NULL;

	return new_node;
}

node *create_snake(int y, int x, int length)
{
	node *head         = create_node(y, x);
	node *current_node = head;

	for (int i = 1; i < length; i++)
	{
		node *new_node     = create_node(y, x - i);
		current_node->next = new_node;
		current_node       = new_node;
	}

	return head;
}

node *node_at(node *head, int index)
{
	int   counter = index;
	node *current = head;
	while (current->next != NULL && index != 0)
	{
		index--;
		current = current->next;
	}

	return current;
}

int touching_snake(node *head, int x, int y)
{
	node *current_node = head->next;
	while (current_node != NULL)
	{
		if (current_node->x == x && current_node->y == y)
			return 1;
		current_node = current_node->next;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	const int FRAMERATE       = 25;
	const int DEFAULT_SNAKE_X = 10;
	const int DEFAULT_SNAKE_Y = 10;

	char input;
	int  last_node_x;
	int  last_node_y;
	int  term_x;
	int  term_y;

	int apple_x      = 0;
	int apple_y      = 0;
	int exit_code    = 0;
	int score        = 0;
	int snake_length = 10;
	int snake_vel_x  = 1;
	int snake_vel_y  = 0;

	// Ncurses initialisation
	initscr();
	noecho();
	timeout(0);
	curs_set(0);

	// Seed RNG
	srand(time(NULL));

	node *head = create_snake(DEFAULT_SNAKE_Y, DEFAULT_SNAKE_X, snake_length);

	// Sets term_y and term_x
	getmaxyx(stdscr, term_y, term_x);

	do
	{
		apple_y = rand() % term_y;
		apple_x = rand() % term_x;
	} while (touching_snake(head, apple_x, apple_y));

	while (input != 'q')
	{
		// Resizes are no problem
		getmaxyx(stdscr, term_y, term_x);

		// User input
		int modifier = 1;
		switch (input = getch())
		{
		case 'w':
			modifier *= -1;
		case 's':
			if (snake_vel_y == 0)
			{
				snake_vel_x = 0;
				snake_vel_y = modifier;
			}
			break;

		case 'a':
			modifier *= -1;
		case 'd':
			if (snake_vel_x == 0)
			{
				snake_vel_x = modifier;
				snake_vel_y = 0;
			}
			break;
		}

		// Update Positions
		{
			last_node_y = node_at(head, snake_length)->y;
			last_node_x = node_at(head, snake_length)->x;

			for (int i = snake_length - 2; i >= 0; i--)
			{
				node *current = node_at(head, i);
				node *next    = current->next;
				next->y       = current->y;
				next->x       = current->x;
			}
			head->y += snake_vel_y;
			head->x += snake_vel_x;

			if (head->x >= term_x)
				head->x -= term_x;
			else if (head->x < 0)
				head->x += term_x;

			if (head->y >= term_y)
				head->y -= term_y;
			else if (head->y < 0)
				head->y += term_y;
		}

		// Collision Detection
		{
			// Check head and apple
			if (head->x == apple_x && head->y == apple_y)
			{
				do
				{
					apple_y = rand() % term_y;
					apple_x = rand() % term_x;
				} while (touching_snake(head, apple_x, apple_y));

				node *current_node = node_at(head, snake_length);

				node *new_node = create_node(current_node->y, current_node->x);
				current_node->next = new_node;

				snake_length++;
				score++;
			}

			// Check snake against self
			if (touching_snake(head->next, head->x, head->y))
				input = 'q';
		}

		// Draw
		{
			// Snake
			mvprintw(last_node_y, last_node_x, " ");
			for (int i = 0; i < snake_length; i++)
			{
				node *current = node_at(head, i);
				mvprintw(current->y, current->x, "#");
			}

			// Apple
			mvprintw(apple_y, apple_x, "@");
		}
		nanosleep(sleeptime, NULL);
	}
	endwin();
	printf("Game Over! Final Score: %d\n", score);

	return exit_code;
}
