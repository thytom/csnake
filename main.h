#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

// Needed here for timespec struct
const int FRAMERATE = 25;

// Snake Variables
typedef struct node
{
	int          x;
	int          y;
	struct node *next;
} node;

// Declared here to avoid syntax highlight bug that hurts my OCD
const struct timespec sleeptime[] = {
	{0, 1000000000 / FRAMERATE} // Seconds, Nanoseconds
};
